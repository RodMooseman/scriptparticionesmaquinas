if(scriptletInput.includes("is not recognized") == true)
{
scriptletResult = "<span style=\"color:#66cdaa;\"><b>No se pudo obtener la información de este punto</b></span>"
}
else
{
var some = "";
var someArray = {};
// Recover RAM
thatPlace = scriptletInput.indexOf("echo -e");
thatPlace = scriptletInput.indexOf("<RAM>",thatPlace);
otherPlace = scriptletInput.indexOf("<ENDRAM>",thatPlace);
RAM = scriptletInput.substr(thatPlace+6,otherPlace-thatPlace-7);
if(RAM!="")
  RAM=Math.round(RAM/1024/1024);
else
{
  RAM=0;
  some += "Error al recuperar la RAM , se utilizara 0 como tamaño de SWAP y /var/crash\n";
}
// Recover Name
thatPlace = scriptletInput.indexOf("echo -e");
thatPlace = scriptletInput.indexOf("<NAME>",thatPlace);
otherPlace = scriptletInput.indexOf("<NAMELESS>",thatPlace);
hostName = scriptletInput.substr(thatPlace+8,otherPlace-thatPlace-9);
if(hostName=="")
  some += "El nombe del equipo no pudo ser recuperado , no se comprobaran los discos de base de datos.\n";
// Recover list of disk
thatPlace = scriptletInput.indexOf("echo -e");
thatPlace = scriptletInput.indexOf("<INICIO>",thatPlace);
otherPlace = scriptletInput.indexOf("<FIN>",thatPlace);
some2 = scriptletInput.substr(thatPlace+8,otherPlace-thatPlace-9);
if(some2=="")
  some += "No se recuperaron datos sobre las particiones , revisar permisos.\n";
else
{
  // Check fstab
  thatPlace = scriptletInput.indexOf("<INICIO>",otherPlace);
  otherPlace = scriptletInput.indexOf("<FIN>",thatPlace);
  stabber = scriptletInput.substr(thatPlace+8,otherPlace-thatPlace-9);
  if(stabber=="")
    some += "No se recupero la info del archivo /etc/fstab.\n";
  // prepare array
  some2=some2.trim();
  temp = some2.split("\n");
  some3 = new Array(temp.length);
  temp2="";
  for (i=0;i<temp.length;i++)
  {
    some3[i] = temp[i].split(",");
    if(some3[i][0].substr(0,2)=="sd")
      temp2=some3[i][0];
    some3[i][4]=temp2;
  }
  // listar discos:
  for(runner=0;runner<temp.length;runner++)
  {
    if(some3[runner][2]=="disk")
    {
      useD = some3[runner][0];
      diskInf = "Disco "+some3[runner][0]+" de tamaño "+some3[runner][1]+".<br>";
      runner++;
      while(some3[runner][2]=="part")
      {
        diskInf += "    -   incluye la partición "+some3[runner][0]+" de tamaño "+some3[runner][1]+"<br>";
        runner++;
      }
      if(some3[runner][2]!="disk")
      {
        checker5 = some3[runner][0].indexOf("-",0);
        if(0<checker5)
        {
          diskInf += "    -    El disco se usa para "+some3[runner][0].substr(0,checker5)+"<br>";
          if(some3[runner][0].substr(0,checker5)!="APP_vg"&&some3[runner][0].substr(0,checker5)!="ASM_vg"&&some3[runner][0].substr(0,checker5)!="ACFS_vg"&&some3[runner][0].substr(0,checker5)!="OS_vg"&&some3[runner][0].substr(0,checker5)!="OPENV_vg"&&some3[runner][0].substr(0,checker5)!="ORA_vg")
            diskInf += "     -   El disco tiene un VG que no cumple con el estandar o es de mysql <br> vg:"+some3[runner][0].substr(0,checker5)+"<br>";
        }
        else
        {
          diskInf += "     -   El disco tiene un VG que no cumple con el estandar o es de mysql <br> ejemplo de vg en disco:"+some3[runner][0]+"<br>";
        }
        if(some3[runner][2]!="lvm")
          diskInf += "     -  disco "+some3[runner][0]+" es de un tipo diferente a lvm<br>"; 
      }
      else
      runner=runner-1;
      someArray[useD] = diskInf;
    }
  }
  // Check OS Disk
  try
  {
    temp2=searchSub(temp.length,some3,0,5,"OS_vg");
    temp2=some3[temp2][4].substr(0,3);
    temp2=findDiv(temp.length,some3,0,temp2);
    some4 = "Disco SO :<table class=\"fromRod\"><tbody><tr><td >SO</td><td >Disco</td><td>";
    some4 += isThisOfTheSize(80,some3[temp2][1]);
    some4 += "</td><td>";
    some4 += typePar("disk",some3[temp2][2]);
    actPart=some3[temp2][0];
  }
  catch(error)
  {
    some4 = "Disco SO :<table class=\"fromRod\"><tbody><tr><td >Etiqueta OS_vg <br>o disco SO <br>no localizados</td><td >Particion</td><td>";
    some4 += isThisOfTheSize(80,"0G");
    some4 += "</td><td>";
    some4 += typePar("disk","No localizado <br>");
    actPart = "No comprobable";
    delete error;
  }
  some4 += "</td><td > VG</td><td>Cumple al: ";
  some4 += CMP(some4,2);
  some4 += "</td></tr>";
  //disco1 part 1
  some4 += "<tr><td>";
  try
  {
    temp2 = findDiv(temp.length,some3,3,"/boot");
    some += someArray[some3[temp2][4].substr(0,3)];
    some += some4;
    delete someArray[some3[temp2][4].substr(0,3)];
    resTemp = "<b><i>/boot</b></i></td><td>";
    resTemp += disCheck(some3[temp2][4],actPart+"1");
    resTemp += "</td><td>";
    resTemp += isThisOfTheSize(1,some3[temp2][1]);
    resTemp += "</td><td>";
    resTemp += typePar("part",some3[temp2][2]);
  }
  catch(error)
  {
    some += "Disco de SO no localizado/reconocido, revisar lista de discos extra en servidor<br>"
    some += some4;
    resTemp = "<b><i>Particion /boot<br>no localizada.</b></i></td><td>";
    resTemp += disCheck("no localizado",actPart+"1");
    resTemp += "</td><td>";
    resTemp += isThisOfTheSize(1,"0G");
    resTemp += "</td><td>";
    resTemp += typePar("part","no");
    delete error;
  }
  resTemp += "</td><td>VG</td><td>Cumple al:";
  some += resTemp+CMP(resTemp,3)+"</td></tr>"
  //disco1 part 2
  checker1 = ["[SWAP]","/","/var","/opt","/home","/root/plataforma_OYM","/var/log","/var/log/audit","/usr","/tmp","/var/crash"];
  if(RAM<16)
    temp2=RAM;
  else
    temp2=16;
  checker2 = [temp2,5,6,5,5,3,3,1,5,5,RAM];
  for(runner=0;runner<checker1.length;runner++)
  {
    try
    {
      resTemp = "<tr><td><b><i>"+checker1[runner]+"</b></i></td><td>";
      temp2 = findDiv(temp.length,some3,3,checker1[runner]);
      resTemp += disCheck(some3[temp2][4],actPart+"2");
      resTemp += "</td><td>";
      resTemp += isThisOfTheSize(checker2[runner],some3[temp2][1]);
      resTemp += "</td><td>";
      resTemp += typePar("lvm",some3[temp2][2]);
      resTemp += "</td><td>";
      resTemp += checkMount(some3[temp2][0],"OS",some3[temp2][3],stabber);
    }
    catch(error)
    {
      resTemp = "<tr><td><b><i>Particion "+checker1[runner]+"<br> no localizada</b></i></td><td>";
      resTemp += disCheck("no localizado",actPart+"2");
      resTemp += "</td><td>";
      resTemp += isThisOfTheSize(1,"0G");
      resTemp += "</td><td>";
      resTemp += typePar("lvm","no");
      resTemp += "</td><td>";
      resTemp += checkMount("no localizado","OS",checker1[runner],stabber);
      delete error;
    }
    some += resTemp+"</td><td>Cumple al: "+CMP(resTemp,4)+"</td></tr>"
  }
  //disco2
  some += "</tbody></table>";
  try
  {
    some4 = "<br>Disco OPENV:<br><table class=\"fromRod\"><tbody><tr><td >OPENV</td><td >Particion</td><td>";
    temp2=searchSub(temp.length,some3,0,8,"OPENV_vg");
    temp2=some3[temp2][4].substr(0,3);
    temp2=findDiv(temp.length,some3,0,temp2);
    some2=1;
    some += someArray[some3[temp2][4].substr(0,3)];
    delete someArray[some3[temp2][4].substr(0,3)];
    some4 += isThisOfTheSize(12,some3[temp2][1]);
    some4 += "</td><td>";
    some4 += typePar("disk",some3[temp2][2]);
    actPart=some3[temp2][0];
  }
  catch(error)
  {
    some4 = "<br>Disco OPENV:<br><table class=\"fromRod\"><tbody><tr><td >Etiqueta OPENV_vg <br>o disco OPENV <br>no localizados</td><td >Particion</td><td>";
    some4 += isThisOfTheSize(12,"0G");
    some4 += "</td><td>";
    some4 += typePar("disk","undefined");
    actPart = "No comprobable";
    delete error;
  }
  some4 += "</td><td >VG</td><td>Cumple al: ";
  some4 += CMP(some4,2);
  some4 += "</td></tr>";
  //disco2 part 1
  checker1 = ["/usr/openv","/openview"];
  checker2 = [10,2];
  if(some2==1)
    some += some4;
  for(runner=0;runner<checker1.length;runner++)
  {
    try
    {
      resTemp = "<tr><td><b><i>"+checker1[runner]+"</b></i></td><td>";
      temp2 = findDiv(temp.length,some3,3,checker1[runner]);
      if(some2!=1)
      {
        some += someArray[some3[temp2][4].substr(0,3)];
        delete someArray[some3[temp2][4].substr(0,3)];
        some += some4;
        some2=1;
      }
      resTemp += disCheck(some3[temp2][4],actPart+"1");
      resTemp += "</td><td>";
      resTemp += isThisOfTheSize(checker2[runner],some3[temp2][1]);
      resTemp += "</td><td>";
      resTemp += typePar("lvm",some3[temp2][2]);
      resTemp += "</td><td>";
      resTemp += checkMount(some3[temp2][0],"OPENV",some3[temp2][3],stabber);
    }
    catch(error)
    {
      if(some2!=1)
      {
        some += "Disco de OPENV no localizado/reconocido, revisar lista de discos extra en servidor<br>"
        some += some4;
        some2=1;
      }
      resTemp = "<tr><td><b><i>Particion "+checker1[runner]+"<br> no localizada</b></i></td><td>";
      resTemp += disCheck("no localizado",actPart+"1");
      resTemp += "</td><td>";
      resTemp += isThisOfTheSize(1,"0G");
      resTemp += "</td><td>";
      resTemp += typePar("lvm","no");
      resTemp += "</td><td>";
      resTemp += checkMount("no localizado","OPENV",checker1[runner],stabber);
      delete error;
    }
    some += resTemp+"</td><td>Cumple al: "+CMP(resTemp,4)+"</td></tr>"
  }
  some += "</tbody></table>";
  //base de datos?
  if(hostName.substr(-4,2).toLowerCase() == "bd")
  {
    // revisar si es /oracle o /mysql) 
    temp2="";
    try
    {
      temp2 = findDiv(temp.length,some3,3,"/oracle");
      some += someArray[some3[temp2][4].substr(0,3)];
      delete someArray[some3[temp2][4].substr(0,3)];
    }
    catch(error)
    {
      temp2="not oracle";
      delete error;
    }
    //  Es oracle
    if(temp2!="not oracle")
    {
      temp2=some3[temp2][4].substr(0,3);
      some += "<br>Disco Oracle en :"+temp2+"<br><table class=\"fromRod\"><tbody><tr>";
      temp2=findDiv(temp.length,some3,0,temp2);
      resTemp = "<td >Oracle</td><td >Particion</td><td>";
      resTemp += isThisOfTheSize(210,some3[temp2][1]);
      resTemp += "</td><td>";
      resTemp += typePar("disk",some3[temp2][2]);
      actPart=some3[temp2][0];
      resTemp += "</td><td >VG</td><td>Cumple con: ";
      some += resTemp+CMP(resTemp,2);
      some += "</td></tr>";
      checker1 = ["/oraema","/rutinasbd","/descargasbd","/grid","/oracle"];
      checker2 = [3,3,100,50,50];
      for(runner=0;runner<checker1.length;runner++)
      {
        try
        {
          resTemp = "<tr><td><b><i>"+checker1[runner]+"</b></i></td><td>";
          temp2 = findDiv(temp.length,some3,3,checker1[runner]);
          resTemp += disCheck(some3[temp2][4],actPart+"1");
          resTemp += "</td><td>";
          resTemp += isThisOfTheSize(checker2[runner],some3[temp2][1]);
          resTemp += "</td><td>";
          resTemp += typePar("lvm",some3[temp2][2]);
          resTemp += "</td><td>";
          resTemp += checkMount(some3[temp2][0],"ORA",some3[temp2][3],stabber);
        }
        catch(error)
        {
          resTemp = "<tr><td><b><i>Particion "+checker1[runner]+"<br> no localizada</b></i></td><td>";
          resTemp += disCheck("no localizado",actPart+"1");
          resTemp += "</td><td>";
          resTemp += isThisOfTheSize(1,"0G");
          resTemp += "</td><td>";
          resTemp += typePar("lvm","no");
          resTemp += "</td><td>";
          resTemp += checkMount("no localizado","ORA",checker1[runner],stabber);
          delete error;
        }
        some += resTemp+"</td><td>Cumple al: "+CMP(resTemp,4)+"</td></tr>"
      }
      actPart="";
      some += "</tbody></table>";
    }
    //  Es mysql
    else
    {
      try
      {
        temp2 = findDiv(temp.length,some3,3,"/mysql");
        some += someArray[some3[temp2][4].substr(0,3)];
        delete someArray[some3[temp2][4].substr(0,3)];
      }
      catch(error)
      {
        temp2="not mysql";
        delete error;
      }
      if(temp2!="not mysql")
      {
        checker5=some3[temp2][0].indexOf("_vg");
        checker5=some3[temp2][0].substr(0,checker5);
        temp2=some3[temp2][4].substr(0,3);
        some += "<br>Disco MySql en :"+temp2+"<br><table class=\"fromRod\"><tbody><tr>";
        temp2=findDiv(temp.length,some3,0,temp2);
        resTemp = "<td >Mysql.</td><td >Particion</td><td>";
        resTemp += isThisOfTheSize(50,some3[temp2][1]);
        resTemp += "</td><td>";
        resTemp += typePar("disk",some3[temp2][2]);
        actPart=some3[temp2][0];
        resTemp += "</td><td >VG</td><td>Cumple con: ";
        some += resTemp+CMP(resTemp,2);
        some += "</td></tr>";
        checker1 = ["/mysql","/backup","/mo1"];
        checker2 = [10,10,30];
        for(runner=0;runner<checker1.length;runner++)
        {
          try
          {
            resTemp = "<tr><td><b><i>"+checker1[runner]+"</b></i></td><td>";
            temp2 = findDiv(temp.length,some3,3,checker1[runner]);
            resTemp += disCheck(some3[temp2][4],actPart+"1");
            resTemp += "</td><td>";
            resTemp += isThisOfTheSize(checker2[runner],some3[temp2][1]);
            resTemp += "</td><td>";
            resTemp += typePar("lvm",some3[temp2][2]);
            resTemp += "</td><td>";
            resTemp += checkMount(some3[temp2][0],checker5,some3[temp2][3],stabber);
          }
          catch(error)
          {
            resTemp = "<tr><td><b><i>Particion "+checker1[runner]+"<br> no localizada</b></i></td><td>";
            resTemp += disCheck("no localizado",actPart+"1");
            resTemp += "</td><td>";
            resTemp += isThisOfTheSize(1,"0G");
            resTemp += "</td><td>";
            resTemp += typePar("lvm","no");
            resTemp += "</td><td>";
            resTemp += checkMount("no localizado",checker5,checker1[runner],stabber);
            delete error;
          }
          some += resTemp+"</td><td>Cumple al: "+CMP(resTemp,4)+"</td></tr>"
        }
        actPart="";
        some += "</tbody></table>";
      }
    }
  }
  some += "Discos de uso no especificado en el estandar:<br>"
  for (var key in someArray) 
  {
    some += someArray[key];
  }
}
scriptletResult =some;
}
function contarCumple(thatThing)
{
    var r = {};
    thatThing = thatThing.split(/\s+/).map(function(a){ !r[a] && (r[a] = 0); r[a]++ })
    return function(what)
    {
      return {"repeticiones": r}[what]
    }
}
function findDiv(tam,mat1,place,cadena)
{
  for (i=0;i<tam;i++)
  {
    if(mat1[i][place]==cadena)
      return(i);
  }
}
function searchSub(tam,mat1,place,cant,cadena)
{
  for (i=0;i<tam;i++)
  {
    if(mat1[i][place].substr(0,cant)==cadena)
      return(i);
  }
}
function isThisOfTheSize(expect,given)
{
	if(given.substr(-1,1)=="G")
    given2=given.substr(0,given.length-1)*1024;
  else
    given2=given.substr(0,given.length-1);
  if(given2<expect*1024)
    ftemp="Tamaño actual: "+given+"<br>El estandar es: "+expect+"G<br><span style=\"color:#b02121;\"> NO DEACUERDO A ESTANDAR </span>."; 
  else
    ftemp="Tamaño actual: "+given+"<br>El estandar es: "+expect+"G<br><span style=\"color:#2180b0;\"> CUMPLE CON EL ESTANDAR </span>.";
	return(ftemp);
}
function disCheck(haveThis,needThis)
{
  ftemp="";
	if(haveThis!=needThis)
    ftemp="Montado en: "+haveThis+"<br>Estandar: "+needThis+"<br><span style=\"color:#b02121;\"> NO DEACUERDO A ESTANDAR </span>."; 
  else
    ftemp="Montado en: "+haveThis+"<br>Estandar: "+needThis+"<br><span style=\"color:#2180b0;\"> CUMPLE CON EL ESTANDAR </span>.";
	return(ftemp);
}
function typePar(haveThis,needThis)
{
  if(haveThis!=needThis)
    ftemp="Tipo de particion: "+haveThis+"<br>El estandar es: "+needThis+"<br><span style=\"color:#b02121;\"> NO DEACUERDO A ESTANDAR </span>."; 
  else
    ftemp="Tipo de particion: "+haveThis+"<br>El estandar es: "+needThis+"<br><span style=\"color:#2180b0;\"> CUMPLE CON EL ESTANDAR </span>.";
  return(ftemp);
}
function checkMount(haveThis,preNeed,name,fstab)
{
  needProc=preNeed+"_vg";
  if(name=="/")
    needProc += "-root_vol"
  else if(name=="[SWAP]")
    needProc += "-swap_vol"
  else
  {
    ftemp=name.substr(1,name.length-1);
    ftemp=ftemp.replace(/\//g,"_");
    needProc += "-"+ftemp+"_vol"
  }
  if(haveThis!=needProc)
    ftemp="VG: "+haveThis+"<br>VG estandar: "+needProc+"<br><span style=\"color:#b02121;\"> NO DEACUERDO A ESTANDAR </span>."; 
  else
    ftemp="VG: "+haveThis+"<br>VG estandar: "+needProc+"<br><span style=\"color:#2180b0;\"> CUMPLE CON EL ESTANDAR </span>.";
  if (fstab.includes("/dev/"+haveThis) == true)
    ftemp += "<br>correctamente montado en fstab";
  else if (fstab.includes("/dev/mapper/"+haveThis) == true)
    ftemp += "<br>montado en fstab (incluye mapper)";
  else
    ftemp += "<br><span style=\"color:#b02121;\"> NO SE ENCUENTRA EN fstab </span>";
  return(ftemp);
}
function contarCumple(thatThing)
{
    var r = {};
    thatThing = thatThing.split(/\s+/).map(function(a){ !r[a] && (r[a] = 0); r[a]++ })
    return function(what)
    {
      return {"repeticiones": r}[what]
    }
}
function CMP(datos1,numberOfThings)
{
  datos1=contarCumple(datos1);
  datos1 = datos1("repeticiones")["CUMPLE"];
  if(datos1 == undefined)
    datos1 = 0;
  datos1 = (datos1 / numberOfThings)*100;
  datos1 = datos1.toFixed(0);
  if(datos1<60)
    datos1="<span style=\"color:#b02121;\">"+datos1;
  else if(datos1<80)
    datos1="<span style=\"color:#8c8a20;\">"+datos1;
  else
    datos1="<span style=\"color:#2180b0;\">"+datos1;
  return (datos1+"%</span>");
}